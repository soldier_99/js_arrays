
const cb = (element) => {
    return ++element ;
    
}

function map(elements, cb) {
    newArr = [];
    for(let i = 0; i < elements.length; i++){
        let x = cb(elements[i]);
        newArr.push(x);

    }
    return newArr ;
}

 module.exports = {
      map,
      cb
 };
