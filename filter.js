let cb = element =>{
    let a
    element < 5 ? a = true: a = false ;
    return a ;
}

function filter(elements = [], cb) {
    newArr =[]
    for (let i = 0; i<elements.length;i++){
        let a = cb(elements[i]);
        if (a) {
            newArr.push(elements[i]);
        }
    }
    return newArr;
    // Similar to `find` but you will return an array of all elements that passed the truth test
}
module.exports.a = filter
module.exports.b = cb 