
const cb = (element,index) => {
    console.log(2*element);
    
}
    // Iterates over a list of elements, yielding each in turn to the `cb` function.


function each(elements = [], cb) {
    for(let i = 0; i < elements.length; i++){
         cb(elements[i],i);

    }
}

 module.exports = {
     fna: each,
     fnb: cb
 };


/* NOTE: Following methods are also working - */

// module.exports = {
//         each,
//         cb
//     };
// module.exports.fn1 = each;
// module.exports.fn2 = cb ;